import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns

class StudentDataAnalysis:
    def __init__(self, file_name, delimiter=';'):
        self.file_name = file_name
        self.delimiter = delimiter
        self.data = pd.read_csv(self.file_name, delimiter=self.delimiter)
        self.numeric_summary = None
        self.categorical_summary = None

    def calculate_period_averages(self):
        mean_G1 = self.data['G1'].mean()
        mean_G2 = self.data['G2'].mean()
        mean_G3 = self.data['G3'].mean()
        return mean_G1, mean_G2, mean_G3
    def calculate_overall_average(self):
        self.data['average_grade'] = self.data[['G1', 'G2', 'G3']].mean(axis=1)
        overall_average = self.data['average_grade'].mean()
        return overall_average

    def compute_basic_statistics(self):
        self.numeric_summary = self.data.describe()
        self.categorical_summary = self.data.describe(include='object')

    def plot_final_grade_distribution(self):
        plt.figure(figsize=(8, 6))
        sns.histplot(self.data['G3'], kde=True)
        plt.title('Rozložení výsledné známky (G3)')
        plt.xlabel('Výsledná známka (G3)')
        plt.ylabel('Počet')
        plt.grid(True)
        plt.show()

    def analyze_parental_education(self):
        plt.figure(figsize=(10, 6))
        sns.boxplot(x='Medu', y='G3', data=self.data)
        plt.title("Vliv nejvyššího dosaženého vzdělaní matky na výslednou známku (G3)")
        plt.xlabel("Nejvyšší dosažené vzdělaní matky")
        plt.ylabel("Výsledná známka (G3)")
        plt.grid(True)
        plt.show()
        plt.figure(figsize=(10, 6))
        sns.boxplot(x='Fedu', y='G3', data=self.data)
        plt.title("Vliv nejvyššího dosaženého vzdělaní otce na výslednou známku (G3)")
        plt.xlabel("Nejvyšší dosažené vzdělaní otce")
        plt.ylabel("Výsledná známka (G3)")
        plt.grid(True)
        plt.show()

    def analyze_family_status(self):
        plt.figure(figsize=(8, 6))
        sns.boxplot(x='Pstatus', y='G3', data=self.data)
        plt.title("Vliv stavu rodiny na výslednou známku (G3)")
        plt.xlabel("Stav rodiny (A = rozvedeni, T = spolu)")
        plt.ylabel("Výsledná známka (G3)")
        plt.grid(True)
        plt.show()

    def analyze_family_support(self):
        plt.figure(figsize=(8, 6))
        sns.boxplot(x='famsup', y='G3', data=self.data)
        plt.title("Vliv rodinné podpory na výslednou známku (G3)")
        plt.xlabel("Podpora rodiny (Ano/Ne)")
        plt.ylabel("Výsledná známka (G3)")
        plt.grid(True)
        plt.show()

    def run_analysis(self):
        self.compute_basic_statistics()
        self.plot_final_grade_distribution()

    def run_family_factors_analysis(self):
        self.analyze_parental_education()
        self.analyze_family_status()
        self.analyze_family_support()

file_name = r'C:\Users\Tadeáš\Documents\skola\ppy1-2024\projects\baldatad\Data_analytic\student-mat.csv'
analysis = StudentDataAnalysis(file_name)
analysis.run_analysis()
analysis.run_family_factors_analysis()

period_averages = analysis.calculate_period_averages()
print("Průměrné známky v jednotlivých obdobích:", period_averages)

overall_average = analysis.calculate_overall_average()
print("Celkový průměr za celý rok:", overall_average)
