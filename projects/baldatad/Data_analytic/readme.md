# Analýza známek matematiky ze středních škol v Portugalsku

## Úvod
Téma finálního projektu pro PPY1 autor zvolil na základě svého zájmu o datovou analýzu. Pro jeho první větší projekt vybral dataset, který obsahuje informace, které jsou snadno představitelné v každodenním životě. Pro zpracování dat byl použit Python a následující knihovny: Pandas, Seaborn, Matplotlib a os.

## Hodnocení v Portugalsku
Pro lepší pochopení je vhodné na začátek uvést systém známkování v Portugalsku, který se od českého systému značně liší. Používá se zde stupnice od 0 do 20 bodů, kde 20 je nejlepší výsledek a 0 nejhorší.

### Stupnice:
* 18-20: Výborný (Excelente)
* 14-17: Velmi dobrý (Bom)
* 10-13: Dobrý (Suficiente)
* 0-9: Nedostatečný (Insuficiente)

### Známka
Známky G1, G2 a G3 představují následující hodnocení:
* **G1:** První známka studenta
* **G2:** Druhá známka studenta
* **G3:** Závěrečná známka na konci školního roku.

## Analýza výsledků studentů

### Průměrné známky během roku
Pomocí metody `calculate_period_averages` byla vypočtena průměrná známka studentů za každý půlrok. Data ukazují na určitý pokles výkonu studentů v průběhu školního roku:
- **Průměrná známka za G1:** 10.91
- **Průměrná známka za G2:** 10.71
- **Průměrná známka za G3:** 10.42


Z těchto hodnot lze pozorovat, že průměrná známka se v průběhu roku postupně snižuje, což může naznačovat různé faktory, jako je například zvyšující se obtížnost učiva nebo pokles motivace studentů.
### Rozložení závěrečných známek (G3)
Rozložení závěrečných známek (G3) mezi studenty bylo analyzováno pomocí histogramu. Nejčastější závěrečné známky se pohybují v rozmezí 10-12, což odpovídá hodnocení "Dobrý" (Suficiente). Přibližně 50 % studentů dosáhlo známky mezi 10 a 13.
#### Hustota rozložení dat
Křivka hustoty rozložení dat (KDE) ukazuje pravděpodobnost, že student dostane danou známku. Vrchol naší křivky je mezi body 10-12, takže výsledek dosti koresponduje s průměrem. Zároven z ní můžeme vyčíst, že je větší šance, že studentův výsledek bude horší, protože křivka má vyšší koncentraci v rozmezí 0-10.
![Graf rozložení známek](./Figure_1.png "Rozložení výsledné známky")

### Celkový průměr za celý rok
Metoda `calculate_overall_average` vypočítala celkový průměr známek za celý rok. Tento ukazatel poskytuje přehled o celkovém výkonu studentů v průběhu školního roku:

- **Celkový průměr za celý rok:** 10.68

Tento průměr naznačuje, že většina studentů dosahuje přibližně průměrných výsledků, což odpovídá hodnocení "Dobrý" (Suficiente).

## Vliv rodiny na výsledky dítěte
Rodinné faktory mohou mít významný vliv na výsledky studentů. Byly analyzovány různé aspekty, jako je vzdělání rodičů, rodinný stav a finanční podpora, a jejich vliv na závěrečné známky studentů.

### Nejvyšší dosažené vzdělání otce
![Graf nejvyššího dosažené vzdělání otce](./Figure_3.png "Nejvyšší dosažené vzdělání otce")


Nejvyšší dosažené vzdělání otce má pozitivní korelaci se závěrečnými známkami studentů. Například:

- **0 (Žádné vzdělání):** Průměrná známka 12.5
- **1 (Základní vzdělání):** Průměrná známka 10.1
- **2 (5. až 9. třída základní školy):** Průměrná známka 10.8
- **3 (Střední vzdělání):** Průměrná známka 11.2
- **4 (Vysokoškolské vzdělání):** Průměrná známka 12.0

Tyto výsledky naznačují, že vyšší dosažené vzdělání otce může být spojeno s lepšími akademickými výsledky jeho dětí.

### Nejvyšší dosažené vzdělání matky
![Graf nejvyššího dosažené vzdělání matky](./Figure_2.png "Nejvyšší dosažené vzdělání matky")


Podobně jako u otce, i vzdělání matky hraje významnou roli ve výsledcích studentů:

- **0 (Žádné vzdělání):** Průměrná známka 8.74
- **1 (Základní vzdělání):** Průměrná známka 10.22
- **2 (5. až 9. třída základní školy):** Průměrná známka 10.9
- **3 (Střední vzdělání):** Průměrná známka 11.25
- **4 (Vysokoškolské vzdělání):** Průměrná známka 11.48

Tyto údaje podporují tvrzení, že vyšší dosažené vzdělání rodičů pozitivně ovlivňuje akademické výsledky dětí.

### Stav rodiny (Pstatus)
![Graf vlivu stavu rodiny](./Figure_4.png "Stav rodiny")


Rodinný stav, tedy zda jsou rodiče spolu (T) nebo rozvedení (A), také hraje roli ve výsledcích studentů:

- Studenti, jejichž rodiče jsou spolu (T), dosahovali průměrné závěrečné známky **10.80**.
- Studenti, jejichž rodiče jsou rozvedení (A), dosahovali průměrné závěrečné známky **10.30**.

Rozdíl v průměrných známkách naznačuje, že studenti z kompletních rodin mohou mít mírně lepší akademické výsledky.

### Finanční podpora (famsup)
![Vliv finanční podpory](./Figure_5.png "Finanční podpora")


Finanční podpora rodiny má také vliv na výsledky studentů:

- Studenti, kteří měli finanční podporu od rodiny ("yes"), dosahovali průměrné závěrečné známky **10.73**.
- Studenti bez finanční podpory od rodiny ("no"), dosahovali průměrné závěrečné známky **10.27**.

Tato data ukazují, že finanční podpora může pozitivně ovlivnit akademické výsledky studentů, což může být spojeno s menším stresem a lepšími podmínkami pro studium.

## Závěr
Z datasetu by se dalo vyčíst mnohem více informací, ale některé věci by byly nad schopnostmi autora. Na základě provedené analýzy je však zřejmé, že různé rodinné faktory, jako je vzdělání rodičů, rodinný stav a finanční podpora, mají značný vliv na výkon studentů. Tato analýza poskytuje základní náhled na vliv těchto faktorů a může sloužit jako základ pro další, podrobnější studie.

## Zdroje
Dataset byl získán na stránkách [Kalifornské univerzity v Irvine](https://archive.ics.uci.edu/dataset/320/student+performance).
