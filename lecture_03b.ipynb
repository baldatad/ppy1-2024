{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "f52f2f79",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "![Morpheus](https://cdn-media-1.freecodecamp.org/images/1*exgznl7z65gttRxLsMAV2A.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7ed8199c",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Functional programming\n",
    "\n",
    "Focus: [functional programming](https://en.wikipedia.org/wiki/Functional_programming) in Python\n",
    "* lambda functions\n",
    "* `map()`\n",
    "* `filter()`\n",
    "* `reduce()`\n",
    "* `zip()`"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e33d9a41",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Functional programming\n",
    "\n",
    "Functional programming is based on the idea of applying mathematical functions to data, where the function takes input arguments and produces an output value, without any side effects. In functional programming, the program flow is controlled by composing functions and passing data between them.\n",
    "\n",
    "The main advantage of functional programming over imperative programming is that it is often more declarative, concise, and easier to reason about. Functional programs are often easier to parallelize and more resilient to errors, as they don't rely on shared mutable state.\n",
    "\n",
    "Functional programming promotes immutability, where data cannot be changed after it's created. This can help avoid unintended side effects and make the code more predictable, which is crucial in data processing where maintaining data integrity is important."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "444aa790",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Lambda functions\n",
    "\n",
    "Lambda functions are anonymous functions that can be defined inline without a name. They are commonly used in functional programming to create small, simple functions that can be passed as arguments to other functions.\n",
    "\n",
    "Lambda functions are useful when you need to define a simple function that is only used once and does not require a name, making your code more concise and readable."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ddfd316f",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Syntax:\n",
    "```\n",
    "lambda arguments: expression\n",
    "```\n",
    "\n",
    "Here, arguments are the input parameters for the function, and expression is a single expression that the lambda function will evaluate. The result of the lambda function is the value of the expression."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "41a8cc88",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "15\n"
     ]
    }
   ],
   "source": [
    "# example:\n",
    "xyz = lambda x, y: x + y\n",
    "result = xyz(5, 10)\n",
    "print(result) # Output: 15"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f5b5726b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Lambda functions can be used to define custom sorting orders for data structures like lists, tuples, and dictionaries. For example, to sort a list of tuples by the second element, you can use a lambda function as the key function for the `sorted()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "25abb6ef",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[(3, 'a'), (2, 'b'), (1, 'c')]\n"
     ]
    }
   ],
   "source": [
    "my_list = [(2, 'b'), (1, 'c'), (3, 'a')]\n",
    "sorted_list = sorted(my_list, key=lambda x: x[1])\n",
    "print(sorted_list) # Output: [(1, 'a'), (2, 'b'), (3, 'c')]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "a58bc21a",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[(1, 'c'), (2, 'b'), (3, 'a')]\n"
     ]
    }
   ],
   "source": [
    "def get_key(t):\n",
    "    return t[0]\n",
    "\n",
    "my_list = [(2, 'b'), (1, 'c'), (3, 'a')]\n",
    "sorted_list = sorted(my_list, key=get_key)\n",
    "print(sorted_list) # Output: [(1, 'a'), (2, 'b'), (3, 'c')]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4cb23f4e",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Besides $\\lambda$\n",
    "\n",
    "The `map()` function is used to apply a given function to each element of an iterable and return a new iterable with the results. The `filter()` function is used to create a new iterable by applying a given function to each element of an iterable and only including elements that satisfy a certain condition. The `reduce()` function is used to apply a given function to an iterable to reduce it to a single value.\n",
    "\n",
    "Lastly `zip()` is a built-in function in Python that is often used in functional programming to combine multiple lists into a single list of tuples. The zip() function takes two or more iterables as arguments and returns an iterator of tuples, where the i-th tuple contains the i-th element from each of the iterables."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bf7efab2",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "For example... let's write code that takes a list of numbers as input and uses the `map()` function to create a new list that contains the square of each number in the original list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "521d607f",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[1, 4, 9, 16, 25]\n"
     ]
    }
   ],
   "source": [
    "numbers = [1, 2, 3, 4, 5]\n",
    "squares = list(map(lambda x: x**2, numbers))\n",
    "print(squares) # Output: [1, 4, 9, 16, 25]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dce7064e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Write a code that takes a list of numbers as input and uses the `filter()` function to create a new list that only contains the even numbers in the original list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "8cc9892b",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "[2, 4]\n"
     ]
    }
   ],
   "source": [
    "numbers = [1, 2, 3, 4, 5]\n",
    "even_numbers = list(filter(lambda x: x % 2 == 0, numbers))\n",
    "print(even_numbers) # Output: [2, 4]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "90c257fb",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Write a code that takes a list of numbers as input and uses the `reduce()` function to calculate the product of all the numbers in the list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "e634d3d8",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "120\n"
     ]
    }
   ],
   "source": [
    "from functools import reduce\n",
    "numbers = [1, 2, 3, 4, 5]\n",
    "product = reduce(lambda x, y: x*y, numbers)\n",
    "print(product) # Output: 120"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "be61f51c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Last, but not least, these concepts are used eg. in list comprehension, which implements the logical principles of functional programming as well."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "01d8e9cc",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "An example of the `zip()` function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c19bd74d",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "['1/a', '2/c', '3/b']\n"
     ]
    }
   ],
   "source": [
    "numbers = [1, 2, 3]\n",
    "letters = ('a', 'c', 'b')\n",
    "zipped = [f'{x[0]}/{x[1]}' for x in zip(numbers, letters)]\n",
    "print(zipped) # Output: [(1, 'a'), (2, 'b'), (3, 'c')]"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "f9623cc7",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "...we used `zip()` to combine the numbers list and the letters list into a list of tuples. This can be useful in many scenarios, such as when you need to iterate over two lists in parallel or when you want to create a dictionary from two lists."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba00dc00",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Intro to `itertools`\n",
    "\n",
    "It is a built-in [module](https://docs.python.org/3/library/itertools.html), that should ...\n",
    "\n",
    "> [..] form an “iterator algebra” making it possible to construct specialized tools succinctly and efficiently in pure Python [..]\n",
    "\n",
    "Let's look at some notable functions in the `itertools` module along with brief explanations and examples. There will be a couple of easter eggs along the way — pay attention, and feel free to ask!"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "46e2bae7",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "1. `count(start=0, step=1)`\n",
    "\n",
    "Generates an infinite arithmetic progression starting from `start` with a step of `step`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "0d880e74",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "3\n",
      "5\n",
      "7\n",
      "9\n",
      "11\n"
     ]
    }
   ],
   "source": [
    "from itertools import count\n",
    "\n",
    "for i in count(1, 2):\n",
    "    print(i)\n",
    "    if i==11:\n",
    "        break"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b680b038",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "2. `cycle(iterable)`\n",
    "\n",
    "Repeats the elements of the given iterable indefinitely."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "688d1bf3",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "red\n",
      "green\n",
      "blue\n",
      "red\n",
      "green\n"
     ]
    }
   ],
   "source": [
    "from itertools import cycle\n",
    "\n",
    "colors = cycle(['red', 'green', 'blue'])\n",
    "for _ in range(5):\n",
    "    print(next(colors))"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "228a3735",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "3. `repeat(element, times=None)`\n",
    "\n",
    "Repeats the specified `element` a specified number of `times` or indefinitely if `times` is not provided."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 10,
   "id": "ed451b63",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Hello\n",
      "Hello\n",
      "Hello\n"
     ]
    }
   ],
   "source": [
    "from itertools import repeat\n",
    "\n",
    "for i in repeat('Hello', 3):\n",
    "    print(i)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b83b9417",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "4. `chain(iterable1, iterable2, ...)`\n",
    "\n",
    "Chains together multiple iterables into a single iterable."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 11,
   "id": "f3526c49",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "1\n",
      "2\n",
      "3\n",
      "4\n",
      "5\n",
      "6\n"
     ]
    }
   ],
   "source": [
    "from itertools import chain\n",
    "\n",
    "numbers = chain([1, 2, 3], [4, 5, 6])\n",
    "for num in numbers:\n",
    "    print(num)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "3adcef52",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "5. `combinations(iterable, r)`\n",
    "\n",
    "Generates all possible combinations of length `r` from the elements of the `iterable`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 12,
   "id": "e24604f0",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "('red', 'green')\n",
      "('red', 'blue')\n",
      "('green', 'blue')\n"
     ]
    }
   ],
   "source": [
    "from itertools import combinations\n",
    "\n",
    "colors = ['red', 'green', 'blue']\n",
    "for combo in combinations(colors, 2):\n",
    "    print(combo)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ba292824",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "6. `permutations(iterable, r=None)`\n",
    "\n",
    "Generates all possible permutations of length `r` from the elements of the `iterable`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 13,
   "id": "f822a0fd",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, 2, 3)\n",
      "(1, 3, 2)\n",
      "(2, 1, 3)\n",
      "(2, 3, 1)\n",
      "(3, 1, 2)\n",
      "(3, 2, 1)\n"
     ]
    }
   ],
   "source": [
    "from itertools import permutations\n",
    "\n",
    "numbers = [1, 2, 3]\n",
    "for perm in permutations(numbers):\n",
    "    print(perm)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ff573b4b",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "7. `product(*iterables, repeat=1)`\n",
    "\n",
    "Computes the Cartesian product of input iterables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 14,
   "id": "ed45ee56",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "(1, 1)\n",
      "(1, 2)\n",
      "(1, 3)\n",
      "(1, 4)\n",
      "(1, 5)\n",
      "(1, 6)\n",
      "(2, 1)\n",
      "(2, 2)\n",
      "(2, 3)\n",
      "(2, 4)\n",
      "(2, 5)\n",
      "(2, 6)\n",
      "(3, 1)\n",
      "(3, 2)\n",
      "(3, 3)\n",
      "(3, 4)\n",
      "(3, 5)\n",
      "(3, 6)\n",
      "(4, 1)\n",
      "(4, 2)\n",
      "(4, 3)\n",
      "(4, 4)\n",
      "(4, 5)\n",
      "(4, 6)\n",
      "(5, 1)\n",
      "(5, 2)\n",
      "(5, 3)\n",
      "(5, 4)\n",
      "(5, 5)\n",
      "(5, 6)\n",
      "(6, 1)\n",
      "(6, 2)\n",
      "(6, 3)\n",
      "(6, 4)\n",
      "(6, 5)\n",
      "(6, 6)\n"
     ]
    }
   ],
   "source": [
    "from itertools import product\n",
    "\n",
    "dice = product(range(1, 7), repeat=2)\n",
    "for roll in dice:\n",
    "    print(roll)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d6ad245",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Contrast with OOP"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 15,
   "id": "247fa64f",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Functional approach\n",
    "numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]\n",
    "filtered_numbers = list(filter(lambda x: x % 2 != 0, numbers))\n",
    "squared_numbers = list(map(lambda x: x**2, filtered_numbers))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 16,
   "id": "32cfe4c0",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "class DataProcessor:\n",
    "    def __init__(self, data):\n",
    "        self.data = data\n",
    "\n",
    "    def filter_even(self):\n",
    "        self.data = [x for x in self.data if x % 2 != 0]\n",
    "\n",
    "    def square_numbers(self):\n",
    "        self.data = [x**2 for x in self.data]\n",
    "\n",
    "# Object-oriented approach\n",
    "processor = DataProcessor([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])\n",
    "processor.filter_even()\n",
    "processor.square_numbers()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9a8de20a",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The functional programming approach is more focused on the transformations applied to the data using functions, while the object-oriented programming approach encapsulates the operations within a class, emphasizing the state and behavior of an object."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "99626cce",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Decorator pattern, where function returns a function\n",
    "\n",
    "The decorator pattern in Python often involves defining a function that takes another function as input, modifies or enhances its behavior, and then returns a new function. This is achieved using the `@decorator` syntax in Python.\n",
    "\n",
    "For example:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 17,
   "id": "99878e8e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Calling function add_numbers with arguments (3, 4) and keyword arguments {}.\n",
      "Function add_numbers finished execution with result: 7.\n",
      "Calling function multiply_numbers with arguments () and keyword arguments {'b': 5, 'a': 2}.\n",
      "Function multiply_numbers finished execution with result: 10.\n"
     ]
    }
   ],
   "source": [
    "def log_function_call(func):\n",
    "    def wrapper(*args, **kwargs):\n",
    "        print(f\"Calling function {func.__name__} with arguments {args} and keyword arguments {kwargs}.\")\n",
    "        result = func(*args, **kwargs)\n",
    "        print(f\"Function {func.__name__} finished execution with result: {result}.\")\n",
    "        return result\n",
    "    return wrapper\n",
    "\n",
    "@log_function_call\n",
    "def add_numbers(a, b):\n",
    "    return a + b\n",
    "\n",
    "@log_function_call\n",
    "def multiply_numbers(a, b):\n",
    "    return a * b\n",
    "\n",
    "# Calling the decorated functions\n",
    "sum_result = add_numbers(3, 4)\n",
    "product_result = multiply_numbers(b=5, a=2)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "941974fd",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "In this example, the `log_function_call` decorator adds logging statements before and after the execution of the decorated functions. When you call `add_numbers` or `multiply_numbers`, the decorator prints information about the function call and its result.\n",
    "\n",
    "This is useful because it allows you to **separate concerns** – the core functionality of adding or multiplying numbers is not cluttered with logging statements, yet you can easily add logging to any function you want without modifying its original code.\n",
    "\n",
    "Decorators are powerful tools for code organization and can be used for various purposes, such as memoization, caching, authentication, and more. They contribute to making code modular and more maintainable."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6bb0aa64",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "Further reading on decorators: https://realpython.com/primer-on-python-decorators/"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6b447204",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## A couple tasks for practicing"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "b36d084e",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Task 1: Sort a list of words by their length\n",
    "\n",
    "In this task, you will use functional programming concepts such as map(), sorted(), and lambda functions to sort a list of words by their length."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "b4a4f8d8",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "words = [\"apple\", \"banana\", \"cherry\", \"date\", \"elderberry\", \"fig\"]\n",
    "sorted_words = # insert your code here!\n",
    "print(sorted_words) # ['elderberry', 'banana', 'cherry', 'apple', 'date', 'fig']"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c749d30f",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Task 2: Compute the average of all numbers in a list\n",
    "\n",
    "In this task, you will use functional programming concepts such as reduce(), len(), and lambda functions to compute the average of all numbers in a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "f3a347f5",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "numbers = [1, 2, 3, 4, 5]\n",
    "average = # ...\n",
    "print(average) # 3.0"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "7fa4371c",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Task 3: Compute the sum of squares of all odd numbers in a list\n",
    "\n",
    "In this task, you will use functional programming concepts such as map(), filter(), reduce(), and lambda functions to compute the sum of squares of all odd numbers in a list."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "76f113f6",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]\n",
    "odd_numbers = # ...\n",
    "squares = # ...\n",
    "sum_of_squares = # ...\n",
    "print(sum_of_squares) # 165"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c722bf01",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Task 4: Compute the total length of a list of strings\n",
    "\n",
    "In this task, you will use functional programming concepts such as map(), reduce(), and lambda functions to compute the total length of a list of strings."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "744017fb",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "strings = [\"apple\", \"banana\", \"cherry\", \"date\", \"elderberry\", \"fig\", \"a\"]\n",
    "lengths = # ...\n",
    "total_length = # ...\n",
    "print(total_length) # 35"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "8d209ba5",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Task 5: Remove duplicates and preserve original order of integers \n",
    "\n",
    "In this task, you will use functional programming concepts such as set(), sorted(), and lambda functions to remove duplicates and sort a list of integers."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "fa9945e8",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "numbers = [4, 2, 1, 3, 2, 4, 5, 3, 1]\n",
    "unique_numbers = # hint: you can use numbers.index(x)\n",
    "print(unique_numbers) # [4, 2, 1, 3, 5]"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.10.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
